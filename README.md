# go-calculator
## Getting started
1. Run `go get`
2. start with `go run main.go`
3. open in `http://127.0.0.1:3000/calculate?firstNum={number}&operand={operand}&secondNum={number}`

## list of Operands
1. '+' => %2B
2. '-' => -
3. '*' => %2A
4. '/' => %2F
