package main

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
)

func main()  {
	app := fiber.New()

    app.Get("/", func(c *fiber.Ctx) error {
        return c.SendString("Hello, World!")
    })

	app.Get("/calculate", Calculate)

    app.Listen(":3000")
}

func Calculate(c *fiber.Ctx) error {
	operand := c.Query("operand")
	firstNum, err := strconv.ParseFloat(c.Query("firstNum"), 64)
	secondNum, err := strconv.ParseFloat(c.Query("secondNum"), 64)

	var result float64
	var message string

	if err != nil {
		return c.JSON(&fiber.Map{
			"code":    201,
			"result":  "No result",
			"message": "Invalid numbers",
		})
	}

	switch operand {
	case "+" :
		result = firstNum + secondNum
		message = "Addition Success"
	case "-" :
		result = firstNum - secondNum
		message = "Substraction Success"
	case "*" :
		result = firstNum * secondNum
		message = "Multiply Success"
	case "/" :
		if secondNum == 0 {
			return c.JSON(&fiber.Map{
				"code":    201,
				"result":  "No result",
				"message": "Cannot divide by zero (0)",
			})
		} else {
			result = firstNum / secondNum
			message = "Division Success"
		}
	default :
		return c.JSON(&fiber.Map{
			"code":    201,
			"result":  "No result",
			"message": "Operand not found",
		})
	}

	return c.JSON(&fiber.Map{
		"code":    200,
		"result":  result,
		"message": message,
	})

}